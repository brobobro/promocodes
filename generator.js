// UTILITY FOR GENERATING ARRAY WITH PROMOCODES
// ********************************************
//
// OPTIONS :
// name - name for json file
// count - count of generated promocodes
// length - length of each promocode

const fs = require('fs');
const argvs = require('./argvToObj.js');
const args = argvs(process.argv);

let { name, count, length } = args;
let promos = [];
count = +count;

console.log('GENERATING CODES.....');

while (count >= 0) {
  let hash = Math.random().toString(36).slice(2, (length + 2));
  promos.push(hash);
  count--
}

promos = JSON.stringify(promos);
console.log(`Done. ${promos}`);

fs.writeFile(`./resources/${name}.json`, promos, function (err) {
  if (err) throw err;
  console.log('Saved!');
});
