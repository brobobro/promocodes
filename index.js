const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');

const app = express();
const port = process.env.PORT || 8080;

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

// api for getting json with promocodes
app.get('/promos/:promo', function(req, res) {
  let promo = req.params.promo;
  let file = './resources/' + promo + '.json';

  fs.readFile(file, function(err, data) {
    res.writeHead(200, {'Content-Type': 'application/json'});
    res.write(data);
    res.send();
  });
});

// api for promocode removing
app.post('/sale/:promo', function(req, res) {
  let promoCode = req.body.code;
  let promo = req.params.promo;
  let file = `./resources/${promo}.json`;
  let codes;

  fs.readFile(file, function(err, data) {
    codes = JSON.parse(data);

    for( var i = 0; i < codes.length-1; i++){
      if ( codes[i] === promoCode) {
        codes.splice(i, 1);
      }
    }

    fs.writeFile(file, JSON.stringify(codes), function (err) {
      if (err) throw err;
      console.log('Replaced!');
      res.send('Done.');
    });
  });
});

// static server for web testing. for change settings you must edit ./web/js/main.js
app.use(express.static('./web'));

app.listen(port);
console.log(`Server started! At http://localhost: ${port}`);
