var promoMachine = function () {
  this.getPromos = function () {
    var it = this;

    $('#codes').html('')

    // you can change param after /promos/ to get json which you need
    $.getJSON('/promos/test', function (data) {
      $('#codes').html(it.listGen(data));
    })
  }

  this.sendPromo = function (code) {
    var it = this;

    $.post(
      // for choosing json to delete promocode you must change param after /sale/.
      // if this file named like promos.json you must type /sale/promos
      '/sale/test',
      {
        code: code
      },
      it.onAjaxSuccess.bind(it)
    );
  }

  this.onAjaxSuccess = function () {
    this.getPromos()
  }

  this.listGen = function (arr) {
    var lis = '';
    var oLi = '<li>';
    var cLi = '</li>';

    for (var i = 0; i < arr.length; i++) {
      lis += (oLi + arr[i] + cLi);
    }

    return lis;
  }
}

$(function () {
  var p = new promoMachine();

  p.getPromos()

  $('#send').click(function () {
    var c = $('#code').val();
    p.sendPromo(c);
    $('#code').val('');
  })
})
