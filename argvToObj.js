// UTILITY FOR CREATE OBJECT WITH ARGVS
// ************************************
//
// EXAMPLE:
// node script.js name=test flag foo=bar  ----> argvToObj(process.argv) => {name: 'test', flag: 'flag', foo: 'bar'}

const argvToObj = function (argv) {
  const obj = {};

  argv.forEach(function (val, index, array) {
    let a = val.split('=')
    let type = a.length > 1 ? 'arr' : 'str'

    switch (type) {
      case 'arr':
        obj[a[0]] = a[1]
        break;
      case 'str':
        obj[a[0]] = a[0]
        break;
  }});

  return obj;
}

module.exports = argvToObj;
